import { LaraValidator } from 'lara-validator';
import axios from 'axios';

axios.defaults.baseURL = 'http://2020-it-30-api.org/api';

// 驗證 API request body
const validate = (data, rules) => {
    if (rules && data) {
      const validator = new LaraValidator(rules, data);
      const isValid = validator.valid();
      if (!isValid) {
        const error = new Error('Front end validate error');
        error.number = 422;
        error.validateError = validator.errorMessage;
        throw error;
      }
    }
};


// 遍例要轉為 FormData 的 object
const parsingObject = (formData, data, parentKey) => {
    const isCurlyBracketsObject = data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File);
    if (isCurlyBracketsObject) {
      Object.keys(data).forEach(key => {
        const nextParentKey = (parentKey) ? `${parentKey}[${key}]` : key;
        parsingObject(formData, data[key], nextParentKey);
      });
    } else {
      const value = data || '';
      formData.append(parentKey, value);
    }
};
  
// 將 object 轉為 FormData
const objectToFormData = (data) => {
    const formData = new FormData();
    parsingObject(formData, data);
    return formData;
};
  
// 呼叫 API
const invokeAPI = async (method, uri, data, token) => {
      const headers = {
          Authorization: `Bearer ${token}`,
      };
      switch (method) {
          case 'get':
            return (await axios.get(uri, { headers })).data;
          case 'post':
            return (await axios.post(uri, data, { headers })).data;
          case 'form':
            data = objectToFormData(data);
            return (await axios.post(uri, data, { headers })).data;
      }
};

export default ({ store }, inject) => {
    inject('api', async ({ method, url, body }, data, urlKey = undefined) => {
        const token = store.getters.userToken;
        const validator = body;
        const apiUrl = (urlKey) ? url[urlKey] : url;
        validate(data, validator);
        return await invokeAPI(method, apiUrl, data, token);
    });
} 