export default async ({ store, redirect }) => {
    const hasToken = store.getters.hasToken;
    const isUser = store.getters['user/isUser'];

    if (!(hasToken && isUser)) {
        redirect('/');
    }
};
