import { validators } from '@/configs/api/user';

export default async ({ app, store }) => {
    const hasToken = store.getters.hasToken;
    const hasUser = store.getters['user/hasUser'];

    if (hasToken) {
        if (!hasUser) {
            try {
                const { data } = await app.$api(validators.AccessUserRequest, { userId: null });
                await store.dispatch('user/setUser', data);
            } catch (error) {
                await store.dispatch('removeUserToken');
            }
        }
    }
};