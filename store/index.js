import Cookies from 'js-cookie';
const cookieparser = process.server ? require('cookieparser') : undefined;

const state = () => {
    return {
        userToken: undefined,
    };
};

const actions = {
    nuxtServerInit({ commit }, { req }) {
        const hasCookieInReq = !!req.headers.cookie;
        if (hasCookieInReq) {
            try {
                const allCookies = cookieparser.parse(req.headers.cookie);
                const token = allCookies['auth-token'];
                commit('SET_USER_TOKEN', token);
            } catch (error) {
                // ...
            }
        }
    },
    setUserToken({ commit }, data) {
        commit('SET_USER_TOKEN', data);
    },
    removeUserToken({ commit }) {
        commit('REMOVE_USER_TOKEN');
    },
};

const mutations = {
    ['SET_USER_TOKEN'](state, data) {
        Cookies.set('auth-token', data);
        state.userToken = data;
    },
    ['REMOVE_USER_TOKEN'](state) {
        Cookies.remove('auth-token');
        state.userToken = undefined;
    }
};

const getters = {
    userToken(state) {
        return state.userToken;
    },
    hasToken(state) {
        return (state.userToken) ? true : false;
    }
};

export default {
    state,
    actions,
    mutations,
    getters,
    namespaced: true,
};