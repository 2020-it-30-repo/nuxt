const state = () => {
    return {
        user: {
            name: undefined,
            email: undefined,
            role: undefined,
        },
    };
};

const actions = {
    setUser({ commit }, data) {
        commit('SET_USER', data);
    },
};

const mutations = {
    ['SET_USER'](state, data) {
        state.user = data;
    }
};

const getters = {
    isManager(state) {
        return state.user.role === 'manager';
    },
    isUser(state) {
        return state.user.role === 'user';
    },
    hasUser(state) {
        return !!state.user.role;
    },
    storeUser(state) {
        return state.user;
    },
};

export default {
    state,
    actions,
    mutations,
    getters,
    namespaced: true,
};